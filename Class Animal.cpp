﻿
#include <iostream>

namespare std

int main()
{
	setlocale(LC_ALL, "ru");

	cout << "Проверка правильности вывода информации о наследниках:" << endl << endl;

	Dog bark;
	bark.Voice();

	Cat soften;
	soften.Voice();

	Bird tweet;
	tweet.Voice();

	cout << endl << "Выполнение работы с использованием массива:" << endl << endl;

		Dog* pbark = &bark;
	Cat* psoften = &soften;
	Bird* ptweet = &tweet;

	int size = 3;

	Animal* arr = new Animal[size];
	{
		arr[0] = *pbark;
		arr[1] = *psoften;
		arr[2] = *ptweet;

		for (int i = 0; i < size; i++)
		{
			arr[i].Voice();
		}

		delete[] arr;
	}

	return 0;
}